package fr.ulille.iut.pizzaland.dto;

public class IngredientDto {
	
	long id;
	String name;

    public IngredientDto() {
        
    }

	public void setId(long id) {
		this.id=id;
	}
	
	public long getId() {
		return this.id;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
}

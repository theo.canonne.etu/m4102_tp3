package fr.ulille.iut.pizzaland.resources;

import java.net.URI;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.dao.IngredientDAO;
import fr.ulille.iut.pizzaland.dto.IngredientCreateDto;
import fr.ulille.iut.pizzaland.dto.IngredientDto;

@Path("/ingredients")
public class IngredientResource {
    private static final Logger LOGGER = Logger.getLogger(IngredientResource.class.getName());
    private IngredientDAO dao;

    @Context
    public UriInfo uriInfo;

    public IngredientResource() {
    	dao = BDDFactory.buildDao(IngredientDAO.class);
    	dao.createTable();
    }
    
 /*@GET
    @Path("{id}")
    public IngredientDto getOneIngredient() {
    	Ingredient ingredient = new Ingredient();
    	ingredient.setId(1);
    	ingredient.setName("mozzarella");
    	
		return Ingredient.toDto(ingredient);
    	
    }*/
    
    @GET
    @Path("{id}")
    public IngredientDto getOneIngredient(@PathParam("id") long id) {
    	LOGGER.info("getOneIngredient(" + id + ")");
    	try {
            Ingredient ingredient = dao.findById(id);
    		return Ingredient.toDto(ingredient);
    	}
        catch ( Exception e ) {
        	// Cette exception générera une réponse avec une erreur 404
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
    }

    @GET
    public List<IngredientDto> getAll() {
        LOGGER.info("IngredientResource:getAll");
        List<IngredientDto> l = dao.getAll().stream().map(Ingredient::toDto).collect(Collectors.toList());

        return l;
    }
    
    @POST
    public Response createIngredient(IngredientCreateDto ingredientCreateDto) {
        Ingredient existing = dao.findByName(ingredientCreateDto.getName());
        if ( existing != null ) {
            throw new WebApplicationException(Response.Status.CONFLICT);
        }
        
        try {
            Ingredient ingredient = Ingredient.fromIngredientCreateDto(ingredientCreateDto);
            long id = dao.insert(ingredient.getName());
            ingredient.setId(id);
            IngredientDto ingredientDto = Ingredient.toDto(ingredient);

            URI uri = uriInfo.getAbsolutePathBuilder().path("" + id).build();

            return Response.created(uri).entity(ingredientDto).build();
        }
        catch ( Exception e ) {
            e.printStackTrace();
            throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
        }
    }
    
    @DELETE
    @Path("{id}")
    public Response deleteIngredient(@PathParam("id") long id) {
    	if ( dao.findById(id) == null ) {
    		throw new WebApplicationException(Response.Status.NOT_FOUND);
    	}

        dao.remove(id);

        return Response.status(Response.Status.ACCEPTED).build();
    }
    
    @GET
    @Path("{id}/name")
    public String getIngredientName(@PathParam("id") long id) {
        Ingredient ingredient = dao.findById(id);
    	if ( ingredient == null ) {
    		throw new WebApplicationException(Response.Status.NOT_FOUND);
    	}
          
    	return ingredient.getName();
    }

    
}
